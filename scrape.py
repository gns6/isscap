import requests
import json
import csv

subjectCode = ["AAAS", "AEROSCI", "AMES", "AMI", "ARABIC", "ARTHIST", "ARTSVIS", "BALTFIN", "BIOETHIC", "BIOLOGY", "BME", "BRAINSOC", "CEE", "CESC", "CHEM", "CHILDPOL", "CHINESE", "CLST", "CMAC", "COMPSCI", "CREOLE", "CULANTH", "DANCE", "DECSCI", "DOCST", "ECE", "ECON", "EDUC", "EHD", "ENGLISH", "ENRGYENV", "ENVIRON", "EOS", "ETHICS", "EVANTH", "FOCUS", "FRENCH", "GBLDEVLP", "GENOME", "GERMAN", "GLHLTH", "GREEK", "GSF", "HCVIS", "HEBREW", "HINDI", "HISTORY", "HOUSECS", "HUMANDEV", "I%26E", "ICS", "IDS", "ISS", "ITALIAN", "JEWISHST", "JPN", "KICHE", "KOREAN", "LATAMER", "LATIN", "LINGUIST", "LIT", "LSGS", "MATH", "ME", "MEDREN", "MILITSCI", "MMS", "MUSIC", "NAVALSCI", "NCS", "NEUROSCI", "PERSIAN", "PHIL", "PHYSEDU", "PHYSICS", "PJMS", "POLISH", "POLSCI", "PORTUGUE", "PSY", "PUBPOL", "QUECHUA", "RELIGION", "RIGHTS", "ROMANIAN", "ROMST", "RUSSIAN", "SANSKRIT", "SCISOC", "SERBCRO", "SES", "SOCIOL", "SPANISH", "STA", "SUSTAIN", "SXL", "THEATRST", "TIBETAN", "TURKISH", "UKRAIN", "UZBEK", "VMS", "WRITING"]
subjectDesc = ["African%20and%20African%20American%20S", "Aerospace%20Studies-AFROTC", "Asian%20%26%20Middle%20Eastern%20Studies", "Arts%20of%20the%20Moving%20Image", "Arabic", "Art%20History", "Visual%20Arts", "Balto-Finnic", "Bioethics%20and%20Science%20Policy", "Biology", "Biomedical%20Engineering", "Brain%20%26%20Society", "Civil%20and%20Environmental%20Egr", "Civic%20Engagement%26Social%20Change", "Chemistry", "Child%20Policy", "Chinese", "Classical%20Studies", "Cmputnl%20Media,%20Arts%20%26%20Cultures", "Computer%20Science", "Creole", "Cultural%20Anthropology", "Dance", "Decision%20Sciences%20Program", "Documentary%20Studies", "Electrical%20%26%20Computer%20Egr", "Economics", "Education", "Education%20and%20Human%20Developmnt", "English", "Energy%20%26%20Environment", "Environment", "Earth%20and%20Ocean%20Sciences", "Study%20of%20Ethics", "Evolutionary%20Anthropology", "Focus", "French", "Global%20Development%20Engineering", "Science%20%26%20Society", "German", "Global%20Health", "Greek", "Gender%20Sexuality%20%26%20Feminist%20St", "Historical%26Cultural%20Visualiztn", "Hebrew", "Hindi", "History", "House%20Course", "Human%20Development", "Innovation%20%26%20Entrepreneurship", "Internatl%20Comparative%20Studies", "Interdisciplinary%20Data%20Science", "Information%20Science%20%2B%20Studies", "Italian", "Jewish%20Studies", "Japanese", "K%E2%80%99iche%20Mayan", "Korean", "Latin%20American%20Studies", "Latin", "Linguistics", "Literature", "Latino%20Studies%20Global%20South", "Mathematics", "Mechanical%20Engr%2FMaterials%20Sci", "Medieval%20and%20Renaissance", "Military%20Science%20(Army%20ROTC)", "Markets%20and%20Management%20Studies", "Music", "Naval%20Science%20(Navy%20ROTC)", "Nonlinear%20and%20Complex%20Systems", "Neuroscience", "Persian", "Philosophy", "Physical%20Education", "Physics", "Policy%20Journalism%20and%20Media%20St", "Polish", "Political%20Science", "Portuguese", "Psychology", "Public%20Policy", "Quechua", "Religion", "Human%20Rights", "Romanian", "Romance%20Studies", "Russian", "SANSKRIT", "Science%20%26%20Society", "Serbian%20and%20Croatian", "Slavic%20and%20Eurasian%20Studies", "Sociology", "Spanish", "Statistical%20Science", "Sustainability%20Engagement", "Study%20of%20Sexualities", "Theater%20Studies", "Tibetan", "Turkish", "Ukrainian", "Uzbek", "Visual%20and%20Media%20Studies", "Writing"]
termCode = [1420, 1430, 1460, 1470, 1500, 1510, 1540, 1550, 1580, 1590, 1620, 1630, 1660, 1670]
termDesc = ["2012%20Fall%20Term", "2013%20Spring%20Term", "2013%20Fall%20Term", "2014%20Spring%20Term", "2014%20Fall%20Term", "2015%20Spring%20Term", "2015%20Fall%20Term", "2016%20Spring%20Term", "2016%20Fall%20Term", "2017%20Spring%20Term", "2017%20Fall%20Term", "2018%20Spring%20Term", "2018%20Fall%20Term", "2019%20Spring%20Term"]

courses = []             # Results of query 1 --> input for query 2
courseOfferings = []     # Results of query 2
classes = []             # Results of query 3
input4 = []              # Results of query 3 --> input for query 4
enrollment = []          # Results of query 4

# GET /curriculum/courses/subject/{subject}
# INPUT: Subject value
# OUTPUT: Full list of course IDs/course offer #s under that subject
def getCourses():
    for i in range(len(subjectCode)):
        print("subj")
        subjectParam = str(subjectCode[i]) + "%20-%20" + subjectDesc[i]
        subjectResponse = requests.get("https://streamer.oit.duke.edu/curriculum/courses/subject/%s?access_token=64c6f6b1045c9b29f52e8c024297a89a" % (subjectParam))
        if (subjectResponse.status_code != 200):
            continue;
        subjectDict = json.loads(subjectResponse.text)
        subjectsFound = subjectDict["ssr_get_courses_resp"]["course_search_result"]
        if (int(subjectsFound["ssr_crs_srch_count"]) < 1):
            continue;
        coursesInSubject = subjectDict["ssr_get_courses_resp"]["course_search_result"]["subjects"]["subject"]["course_summaries"]["course_summary"]
        if (type(coursesInSubject) is dict):
            arrCourseSubj = []
            arrCourseSubj.append(coursesInSubject)
            coursesInSubject = arrCourseSubj
        for crse in coursesInSubject:
            thisCrse = [crse["crse_id"], crse["crse_offer_nbr"], subjectCode[i]]
            courses.append(thisCrse)
            with open('firstCallOutput.csv', mode='ab') as file:
                writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                writer.writerow(thisCrse)
    print("All Courses Cataloged by Subject")

def getCoursesFile():
    with open('firstCallOutput.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            courses.append(row)


# GET /curriculum/courses/crse_id/{crse_id}/crse_offer_nbr/{crse_offer_nbr}
# INPUT: course ID and course offer #
# OUTPUT: course offering info (course components, attributes (MoI,AoK), terms offered)
#         look at various course offer #s for cross listings
def getCourseOfferings():
    with open('coursesBackup.csv', mode='ab') as file:
        header = ["CourseNum", "Course Title", "Subject", "Catalog Num", "Description", "Units", "Consent requried", "Academic Career", "Prereq", "Discussion", "Lecture", "Lab", "Seminar", "Tutorial", "Indep Study", "Focus Sem", "Abroad", "Modes of Inquiry", "Areas of Knowledge", "Cross Listings"]
        writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(header)
    for i in range(len(courses)):
        if (courses[i][0] == 021436): #little blacklist
            continue
        courseParamStr = str(courses[i][0])
        courseOfferNumParamStr = str(courses[i][1])
        courseResponse = requests.get("https://streamer.oit.duke.edu/curriculum/courses/crse_id/%s/crse_offer_nbr/%s?access_token=64c6f6b1045c9b29f52e8c024297a89a" % (courseParamStr, courseOfferNumParamStr))
        if (courseResponse.status_code != 200):
            continue
        courseDict = json.loads(courseResponse.text)
        coursesFound = courseDict["ssr_get_course_offering_resp"]["course_offering_result"]["ssr_terms_offered_count"]
        if (int(coursesFound) < 1):
            continue
        coursesOffering = courseDict["ssr_get_course_offering_resp"]["course_offering_result"]["course_offering"]
        thisCrseOff = []
        thisCrseOff.append(str(courses[i][2]) + str(coursesOffering["catalog_nbr"]).strip())
        courseTitle = coursesOffering["course_title_long"]
        if (not (isinstance(courseTitle, basestring))):
            courseTitle = ""
        thisCrseOff.append(str(courseTitle.encode('ascii', 'ignore')))
        thisCrseOff.append(str(courses[i][2]))
        thisCrseOff.append(str(coursesOffering["catalog_nbr"]).strip())
        descriptionLong = coursesOffering["descrlong"]
        if (not (isinstance(descriptionLong, basestring))):
            descriptionLong = ""
        thisCrseOff.append(str(descriptionLong.encode('ascii', 'ignore')))
        thisCrseOff.append(str(coursesOffering["units_minimum"]))
        thisCrseOff.append(str(coursesOffering["consent"]))
        thisCrseOff.append(str(coursesOffering["acad_career"]))
        prereq = ""
        rqrmntString = str(coursesOffering["rqrmnt_group_descr"])
        if ("Prereq" in rqrmntString):
            prereq = rqrmntString[rqrmntString.find(' ')+1:]
        elif ("None" in rqrmntString):
            prereq = ""
        else:
            prereq = rqrmntString
        thisCrseOff.append(prereq)
        courseComp = coursesOffering["course_components"]["course_component"]
        thisCrseOff.extend([0,0,0,0,0,0,0,0])
        if (type(courseComp) is dict):
            arrCourseComp = []
            arrCourseComp.append(courseComp)
            courseComp = arrCourseComp
        for comp in courseComp:
            if (str(comp["ssr_component"]) == "DIS"):
                thisCrseOff[9] = 1
            if (str(comp["ssr_component"]) == "LEC"):
                thisCrseOff[10] = 1
            if (str(comp["ssr_component"]) == "LAB"):
                thisCrseOff[11] = 1
            if (str(comp["ssr_component"]) == "SEM"):
                thisCrseOff[12] = 1
            if (str(comp["ssr_component"]) == "TUT"):
                thisCrseOff[13] = 1
            if (str(comp["ssr_component"]) == "IND"):
                thisCrseOff[14] = 1
            if ("FS" in str(coursesOffering["catalog_nbr"])):
                thisCrseOff[15] = 1
            if ("A" in str(coursesOffering["catalog_nbr"])):
                thisCrseOff[16] = 1
        modes = ""
        areas = ""
        nullCheck = coursesOffering["course_attributes"]
        if (type(nullCheck) is dict):
            courseAttributes = nullCheck["course_attribute"]
            if (type(courseAttributes) is dict):
                arrCourseAttributes = []
                arrCourseAttributes.append(courseAttributes)
                courseAttributes = arrCourseAttributes
            for att in courseAttributes:
                if (att["crse_attr"] == "CURR"):
                    modes = modes + att["crse_attr_value"] + ", "
                if (att["crse_attr"] == "USE"):
                    areas = areas + att["crse_attr_value"] + ", "
        thisCrseOff.append(modes)
        thisCrseOff.append(areas)
        crossListings = ""
        for j in range(1, int(courses[i][1])) :
            courseOfferNumParamStr = str(j)
            courseResponse = requests.get("https://streamer.oit.duke.edu/curriculum/courses/crse_id/%s/crse_offer_nbr/%s?access_token=64c6f6b1045c9b29f52e8c024297a89a" % (courseParamStr, courseOfferNumParamStr))
            if (courseResponse.status_code != 200):
                continue
            courseDict = json.loads(courseResponse.text)
            coursesFound = courseDict["ssr_get_course_offering_resp"]["course_offering_result"]["ssr_terms_offered_count"]
            if (int(coursesFound) < 1):
                continue
            coursesOffering = courseDict["ssr_get_course_offering_resp"]["course_offering_result"]["course_offering"]
            crossListings = crossListings + str(coursesOffering["subject"]) + str(coursesOffering["catalog_nbr"]).strip() + ", "
        statusCode = 200
        j = int(courses[i][1])
        while (statusCode == 200):
            j = j +1
            courseOfferNumParamStr = str(j)
            courseResponse = requests.get("https://streamer.oit.duke.edu/curriculum/courses/crse_id/%s/crse_offer_nbr/%s?access_token=64c6f6b1045c9b29f52e8c024297a89a" % (courseParamStr, courseOfferNumParamStr))
            statusCode = int(courseResponse.status_code)
            if (courseResponse.status_code != 200):
                continue
            courseDict = json.loads(courseResponse.text)
            coursesFound = courseDict["ssr_get_course_offering_resp"]["course_offering_result"]["ssr_terms_offered_count"]
            if (int(coursesFound) < 1):
                continue
            coursesOffering = courseDict["ssr_get_course_offering_resp"]["course_offering_result"]["course_offering"]
            crossListings = crossListings + str(coursesOffering["subject"]) + str(coursesOffering["catalog_nbr"]).strip() + ", "
        thisCrseOff.append(crossListings)
        print(thisCrseOff)
        courseOfferings.append(thisCrseOff)
        with open('coursesBackup.csv', mode='ab') as file:
            writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(thisCrseOff)
# GET /curriculum/classes/strm/{strm}/crse_id/{crse_id}
# INPUT: term value and course ID and course offer #
# OUTPUT: An entry for each DEPT the course is in (if cross listed, one entry for LIT one for AAAS)
#         course meeting data- when the class runs each week, prof, room, diff sections
def getClasses():
    for j in range(10043, len(courses)):
        for i in range(len(termCode)):
            termParam = str(termCode[i]) + "%20-%20" + termDesc[i]
            courseParamStr = str(courses[j][0])
            courseOfferNumParamStr = str(courses[j][1])
            termCourseResponse = requests.get("https://streamer.oit.duke.edu/curriculum/classes/strm/%s/crse_id/%s?crse_offer_nbr=%s&access_token=64c6f6b1045c9b29f52e8c024297a89a" % (termParam, courseParamStr, courseOfferNumParamStr))
            if (termCourseResponse.status_code != 200):
                continue
            print(courseParamStr)
            print(termParam)
            classDict = json.loads(termCourseResponse.text)
            coursesFound = classDict["ssr_get_classes_resp"]["search_result"]["ssr_course_count"]
            classesFound = classDict["ssr_get_classes_resp"]["search_result"]["ssr_class_count"]
            if (int(coursesFound) < 1):
                continue
            if (int(classesFound) < 1):
                continue
            classesFromDict1 = classDict["ssr_get_classes_resp"]["search_result"]["subjects"]["subject"]
            classesFromDict = classDict["ssr_get_classes_resp"]["search_result"]["subjects"]["subject"]["classes_summary"]["class_summary"]
            if (type(classesFromDict) is dict):
                arrclasses = []
                arrclasses.append(classesFromDict)
                classesFromDict = arrclasses
            for clss in classesFromDict:
                print("clss")
                thisClassInput = []
                thisClassInput.append(termParam)
                thisClassInput.append(courseParamStr)
                thisClassInput.append(courseOfferNumParamStr)
                thisClassInput.append(str(clss["session_code"]))
                thisClassInput.append(str(clss["class_section"]))
                input4.append(thisClassInput)
                with open('input4.csv', mode='ab') as file:
                    writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    writer.writerow(thisClassInput)
                thisClass = []
                thisClass.append(str(courses[j][2]) + str(clss["catalog_nbr"]).strip())
                thisClass.append(clss["strm_lov_descr"])
                thisClass.append(str(clss["class_section"]))
                meetingPattern = clss["classes_meeting_patterns"]["class_meeting_pattern"]
                hasMtg = False
                if (type(meetingPattern) is dict):
                    arrmeetingPattern = []
                    arrmeetingPattern.append(meetingPattern)
                    meetingPattern = arrmeetingPattern
                for mtg in meetingPattern:
                    print("mtg")
                    thisClass.append(mtg["meeting_time_start"])
                    thisClass.append(mtg["meeting_time_end"])
                    thisClass.extend([0,0,0,0,0]) # initialize all days of week to 0
                    if (str(mtg["mon"]) == "Y"):
                        hasMtg = True
                        thisClass[5] = 1
                    if (str(mtg["tues"]) == "Y"):
                        hasMtg = True
                        thisClass[6] = 1
                    if (str(mtg["wed"]) == "Y"):
                        hasMtg = True
                        thisClass[7] = 1
                    if (str(mtg["thurs"]) == "Y"):
                        hasMtg = True
                        thisClass[8] = 1
                    if (str(mtg["fri"]) == "Y"):
                        hasMtg = True
                        thisClass[9] = 1
                    instString = ""
                    instructorNullCheck = mtg["class_instructors"]
                    if (type(instructorNullCheck) is dict):
                        instructors = mtg["class_instructors"]["class_instructor"]
                        if (type(instructors) is dict):
                            arrinst = []
                            arrinst.append(instructors)
                            instructors = arrinst
                        for inst in instructors:
                            print(inst)
                            instString = instString + inst["name_display"].encode('utf-8') + ", "
                    #should have reset hasMtg to false 
                    thisClass.append(instString)
                if not hasMtg:
                    continue
                classes.append(thisClass)
                with open('classes.csv', mode='ab') as file:
                    writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    writer.writerow(thisClass)
'''
#for each class section
"strm_lov_descr"
"class_section"
"classes_meeting_patterns"
    "meeting_time_start" # HH:MM
    "meeting_time_end" # HH:MM
    # each day into a comma separated list
    "class_instructors"
        "class_instructor"
            "name_display" # all prof is comma separated list

#for each class section
"crse_id"
"crse_offer_nbr"
"strm"
"session_code"
"class_section"
'''
def getInput4File():
    with open('input4.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            input4.append(row)
# This one is for enrollment data, but need very specifc search criteria
# GET /curriculum/classes/strm/{strm}/crse_id/{crse_id}/crse_offer_nbr/{crse_offer_nbr}/session_code/{session_code}/class_section/{class_section}
# INPUT: term value, course ID, course offer #, session code, class section
# OUTPUT: enrollment data for that section (caps, actual enrollment #s), textbooks, consent required to enroll or not, prereqs
def getEnrollment():
    for i in range(79882, len(input4)):
        termParam = str(input4[i][0])
        courseParam = str(input4[i][1])
        courseOfferNumParam = str(input4[i][2])
        sessionCodeParam = str(input4[i][3])
        sectionParam = str(input4[i][4])
        termSectionResponse = requests.get("https://streamer.oit.duke.edu/curriculum/classes/strm/%s/crse_id/%s/crse_offer_nbr/%s/session_code/%s/class_section/%s?access_token=64c6f6b1045c9b29f52e8c024297a89a" % (termParam, courseParam, courseOfferNumParam, sessionCodeParam, sectionParam))
        if (termSectionResponse.status_code != 200):
            continue
        print(courseParam)
        print(termParam)
        sectionDict = json.loads(termSectionResponse.text)
        sectionsFound = sectionDict["ssr_get_class_section_resp"]["class_section_result"]["class_sections"]
        if (type(sectionsFound) is not dict):
            continue
        sectionsFromDict = sectionDict["ssr_get_class_section_resp"]["class_section_result"]["class_sections"]["ssr_class_section"]
        if (type(sectionsFromDict) is not dict):
            continue
        thisSection = []
        thisSection.append(sectionsFromDict["subject"] + str(sectionsFromDict["catalog_nbr"]).strip())
        thisSection.append(sectionsFromDict["strm_lov_descr"])
        thisSection.append(sectionsFromDict["session_code_lov_descr"])
        thisSection.append(sectionParam)
        thisSection.append(sectionsFromDict["enrl_cap"])
        thisSection.append(sectionsFromDict["enrl_tot"])
        thisSection.append(sectionsFromDict["wait_tot"])
        thisSection.append(sectionsFromDict["wait_cap"])
        sectionClassNumber = sectionsFromDict["class_nbr"]
        combSecNullCheck = sectionsFromDict["class_combined_section"]
        if (type(combSecNullCheck) is dict):
            combSec = sectionsFromDict["class_combined_section"]["combined_section"]
            if (type(combSec) is dict):
                arrsec = []
                arrsec.append(combSec)
                combSec = arrsec
            for sec in combSec:
                if (int(sec["class_nbr"]) == int(sectionClassNumber)):
                    sectionEnrollment = sec["enrl_tot"]
                    thisSection.append(sectionEnrollment)
                    sectionWait = sec["wait_tot"]
                    thisSection.append(sectionWait)
        else:
            thisSection.append("NA")
            thisSection.append("NA")
        enrollment.append(thisSection)
        with open('enrollment.csv', mode='ab') as file:
            writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(thisSection)


#Main
# getCourses()
getCoursesFile()
# getCourseOfferings()
#getClasses()
getInput4File()
getEnrollment()
