import requests
import json
import csv

subjectCode = ["AAAS", "AEROSCI", "AMES", "AMI", "ARABIC", "ARTHIST", "ARTSVIS", "BALTFIN", "BIOETHIC", "BIOLOGY", "BME", "BRAINSOC", "CEE", "CESC", "CHEM", "CHILDPOL", "CHINESE", "CLST", "CMAC", "COMPSCI", "CREOLE", "CULANTH", "DANCE", "DECSCI", "DOCST", "ECE", "ECON", "EDUC", "EHD", "ENGLISH", "ENRGYENV", "ENVIRON", "EOS", "ETHICS", "EVANTH", "FOCUS", "FRENCH", "GBLDEVLP", "GENOME", "GERMAN", "GLHLTH", "GREEK", "GSF", "HCVIS", "HEBREW", "HINDI", "HISTORY", "HOUSECS", "HUMANDEV", "I%26E", "ICS", "IDS", "ISS", "ITALIAN", "JEWISHST", "JPN", "KICHE", "KOREAN", "LATAMER", "LATIN", "LINGUIST", "LIT", "LSGS", "MATH", "ME", "MEDREN", "MILITSCI", "MMS", "MUSIC", "NAVALSCI", "NCS", "NEUROSCI", "PERSIAN", "PHIL", "PHYSEDU", "PHYSICS", "PJMS", "POLISH", "POLSCI", "PORTUGUE", "PSY", "PUBPOL", "QUECHUA", "RELIGION", "RIGHTS", "ROMANIAN", "ROMST", "RUSSIAN", "SANSKRIT", "SCISOC", "SERBCRO", "SES", "SOCIOL", "SPANISH", "STA", "SUSTAIN", "SXL", "THEATRST", "TIBETAN", "TURKISH", "UKRAIN", "UZBEK", "VMS", "WRITING"]
subjectDesc = ["African%20and%20African%20American%20S", "Aerospace%20Studies-AFROTC", "Asian%20%26%20Middle%20Eastern%20Studies", "Arts%20of%20the%20Moving%20Image", "Arabic", "Art%20History", "Visual%20Arts", "Balto-Finnic", "Bioethics%20and%20Science%20Policy", "Biology", "Biomedical%20Engineering", "Brain%20%26%20Society", "Civil%20and%20Environmental%20Egr", "Civic%20Engagement%26Social%20Change", "Chemistry", "Child%20Policy", "Chinese", "Classical%20Studies", "Cmputnl%20Media,%20Arts%20%26%20Cultures", "Computer%20Science", "Creole", "Cultural%20Anthropology", "Dance", "Decision%20Sciences%20Program", "Documentary%20Studies", "Electrical%20%26%20Computer%20Egr", "Economics", "Education", "Education%20and%20Human%20Developmnt", "English", "Energy%20%26%20Environment", "Environment", "Earth%20and%20Ocean%20Sciences", "Study%20of%20Ethics", "Evolutionary%20Anthropology", "Focus", "French", "Global%20Development%20Engineering", "Science%20%26%20Society", "German", "Global%20Health", "Greek", "Gender%20Sexuality%20%26%20Feminist%20St", "Historical%26Cultural%20Visualiztn", "Hebrew", "Hindi", "History", "House%20Course", "Human%20Development", "Innovation%20%26%20Entrepreneurship", "Internatl%20Comparative%20Studies", "Interdisciplinary%20Data%20Science", "Information%20Science%20%2B%20Studies", "Italian", "Jewish%20Studies", "Japanese", "K%E2%80%99iche%20Mayan", "Korean", "Latin%20American%20Studies", "Latin", "Linguistics", "Literature", "Latino%20Studies%20Global%20South", "Mathematics", "Mechanical%20Engr%2FMaterials%20Sci", "Medieval%20and%20Renaissance", "Military%20Science%20(Army%20ROTC)", "Markets%20and%20Management%20Studies", "Music", "Naval%20Science%20(Navy%20ROTC)", "Nonlinear%20and%20Complex%20Systems", "Neuroscience", "Persian", "Philosophy", "Physical%20Education", "Physics", "Policy%20Journalism%20and%20Media%20St", "Polish", "Political%20Science", "Portuguese", "Psychology", "Public%20Policy", "Quechua", "Religion", "Human%20Rights", "Romanian", "Romance%20Studies", "Russian", "SANSKRIT", "Science%20%26%20Society", "Serbian%20and%20Croatian", "Slavic%20and%20Eurasian%20Studies", "Sociology", "Spanish", "Statistical%20Science", "Sustainability%20Engagement", "Study%20of%20Sexualities", "Theater%20Studies", "Tibetan", "Turkish", "Ukrainian", "Uzbek", "Visual%20and%20Media%20Studies", "Writing"]
termCode = [1420, 1430, 1460, 1470, 1500, 1510, 1540, 1550, 1580, 1590, 1620, 1630, 1660, 1670]
termDesc = ["2012%20Fall%20Term", "2013%20Spring%20Term", "2013%20Fall%20Term", "2014%20Spring%20Term", "2014%20Fall%20Term", "2015%20Spring%20Term", "2015%20Fall%20Term", "2016%20Spring%20Term", "2016%20Fall%20Term", "2017%20Spring%20Term", "2017%20Fall%20Term", "2018%20Spring%20Term", "2018%20Fall%20Term", "2019%20Spring%20Term"]

courses = []
cleaned = []

def getCoursesFile():
    with open('courses.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            courses.append(row)

def clean():
    for i in range(len(courses)):
        thisRow = []
        thisRow.append(courses[i][0])
        thisRow.extend([0,0,0,0,0,0,0,0, 0, 0, 0]) #CZ, SS, ALP, NS, QS, CCI, R, W, EI, STS, FL
        thisRowAoK = courses[i][18]
        thisRowMoI = courses[i][17]
        if ("CZ" in thisRowAoK):
            thisRow[1] = 1
        if ("SS" in thisRowAoK):
            thisRow[2] = 1
        if ("ALP" in thisRowAoK):
            thisRow[3] = 1
        if ("NS" in thisRowAoK):
            thisRow[4] = 1
        if ("QS" in thisRowAoK):
            thisRow[5] = 1
        if ("CCI" in thisRowMoI):
            thisRow[6] = 1
        if ("R" in thisRowMoI):
            thisRow[7] = 1
        if ("W" in thisRowMoI):
            thisRow[8] = 1
        if ("EI" in thisRowMoI):
            thisRow[9] = 1
        if ("STS" in thisRowMoI):
            thisRow[10] = 1
        if ("FL" in thisRowMoI):
            thisRow[11] = 1
        with open('CirCodes.csv', mode='ab') as file:
            writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(thisRow)

#Main
getCoursesFile()
clean()
